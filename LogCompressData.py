import math
import logging
logging.basicConfig(filename="LogFiles/logsInfo.log", filemode='w', level=logging.DEBUG, format='%(asctime)s %(message)s')


def log_compress(signal_data):
    """

    :param signal_data: Data to be processed
    :type signal_data: array
    :returns: log_compressed
    :rtype: array
    """

    logging.debug("Log-compressing data.")
    log_compressed = [None]*len(signal_data)

    for i in range(0, len(signal_data)):
        log = math.log10(10 + signal_data[i])
        log_compressed[i] = log

    logging.info("Log-compressed data:" + str(log_compressed))
    return log_compressed
