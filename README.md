This program takes a binary dataset along with a coinciding JSON file with parameters to show a 2d ultrasound image.

To display options for running the program
From Command line Type:
python main.py --help
