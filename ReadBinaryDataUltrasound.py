import numpy as np
import json
import logging
import sys
logging.basicConfig(filename="LogFiles/logsInfo.log", filemode='w', level=logging.DEBUG, format='%(asctime)s %(message)s')


def get_dimensions(json_file):
    """Calculates and returns lateral and axial locations

    :param json_file:
    :type json_file: str
    :return: dimensions (dictionary of x and y dimensions)
    :rtype: dict

    """

    logging.debug("Calculating lateral and axial locations")
    num_beams = get_num_beams(json_file)
    beam_spacing = get_beam_spacing(json_file)
    speed = get_speed(json_file)
    axial_samples = get_axial_samples(json_file)
    sampling_freq = get_sampling_frequency(json_file)
    dimensions = {}
    lat_locs = [None]*num_beams
    axial_locs = [None]*axial_samples

#   #lateral dimensions
    for i in range(0, num_beams):
        lat_locs[i] = beam_spacing*i
    dimensions['x'] = lat_locs
#   #axial_dimensions
    for i in range(0, axial_samples):
        num = speed*i
        denom = sampling_freq/2
        axial_locs[i] = num/denom
    dimensions['y'] = axial_locs

    logging.info("lateral locations" + str(lat_locs))
    logging.info("axial locations" + str(axial_locs))

    return dimensions


def read_binary_file(binary_file_name):
    """This function opens the binary file and returns an array of the radiofrequency data

    :param binary_file_name: The file to be opened
    :type binary_file_name: str.
    :returns: list of signal data
    :rtype: array

    """
    logging.debug("Opening and reading binary file with radio-frequency data.")
    try:
        file = open(binary_file_name)
        data = np.fromfile(file, dtype='int16', count=-1)
    except IOError:
        logging.error("IO Error. Cannot find file. Process exiting.")
        raise IOError

    logging.info("Radio-frequency data" + str(data))
    return data


def read_json_file(json_file):
    """This function opens the JSON file and returns a dictionary of the metadata in the file.

    :param json_file: The file to be opened
    :type json_file: str.
    :returns: dictionary of metadata
    :rtype: dict

    """

    logging.debug("Opening and reading JSON file to obtain metadata.")
    try:
        with open(json_file) as data_file:
            data = json.load(data_file)
    except IOError:
        logging.error("IO Error. Cannot find file. Process exiting.")
        raise IOError

    logging.info("Metadata:" + str(data))
    return data


def get_speed(json_file):
    """This function opens the JSON file and returns the speed of sound specified in the JSON file

    :param json_file: The file to be opened
    :type json_file: str.
    :returns: speed of sound from JSON file
    :rtype: int

    """

    logging.debug("Getting speed of sound.")
    data = read_json_file(json_file)

    logging.info("Speed of sound:" + str(data["c"]))
    try:
        return data["c"]
    except KeyError:
        logging.error("KeyError: Invalid key, cannot provide speed. Process exiting.")
        raise KeyError


def get_sampling_frequency(json_file):
    """This function opens the JSON file and returns the sampling frequency in the JSON file

    :param json_file: The file to be opened
    :type json_file: str.
    :returns:  sampling frequency from JSON file
    :rtype: int

    """

    logging.debug("Getting sampling frequency.")
    data = read_json_file(json_file)

    logging.info("Sampling frequency" + str(data["fs"]))
    try:
        return data["fs"]
    except KeyError:
        logging.error("KeyError: Invalid key, cannot provide sampling frequency. Process exiting.")
        raise KeyError


def get_axial_samples(json_file):
    """This function opens the JSON file and returns the number of axial samples specified in the JSON file

    :param json_file: The file to be opened
    :type json_file: str.
    :returns:  number of axial samples from JSON file
    :rtype: int

    """

    logging.debug("Getting number of axial samples.")
    data = read_json_file(json_file)

    logging.info("Number of axial samples" + str(data["axial_samples"]))
    try:
        return data["axial_samples"]
    except KeyError:
        logging.error("KeyError: Invalid key, cannot provide axial samples. Process exiting.")
        raise KeyError


def get_beam_spacing(json_file):
    """This function opens the JSON file and returns the beam spacing specified in the JSON file

    :param json_file: The file to be opened
    :type json_file: str.
    :returns:  beam spacing from JSON file
    :rtype: int

    """

    logging.debug("Getting beam spacing.")
    data = read_json_file(json_file)

    logging.info("Beam spacing" + str(data["beam_spacing"]))
    try:
        return data["beam_spacing"]
    except KeyError:
        logging.error("KeyError: Invalid key, cannot provide beam spacing. Process exiting.")
        raise KeyError


def get_num_beams(json_file):
    """This function opens the JSON file and returns the number of lateral beams specified in the JSON file

    :param json_file: The file to be opened
    :type json_file: str.
    :returns: number of lateral beams from JSON file
    :rtype: int

    """

    logging.debug("Getting number of beams.")
    data = read_json_file(json_file)

    logging.info("Number of beams" + str(data["num_beams"]))
    try:
        return data["num_beams"]
    except KeyError:
        logging.error("KeyError: Invalid key, cannot provide number of beams. Process exiting.")
        raise KeyError
