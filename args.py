import argparse as ap
import logging
logging.basicConfig(filename="LogFiles/logsInfo.log", filemode='w', level=logging.DEBUG, format='%(asctime)s %(message)s')


def getargs():
    """Get the arguments

    :return: input arguments as args

    """

    logging.debug("Obtaining input arguments.")
    par = ap.ArgumentParser(description="Whatever arguments we need",
                            formatter_class=ap.ArgumentDefaultsHelpFormatter)

    par.add_argument("--json",
                     dest="jsonfile",
                     help="name of the JSON file",
                     type=str,
                     default="bmode.json")

    par.add_argument("--rf",
                     dest="rfdata",
                     help="name of the RF data file",
                     type=str,
                     default="rfdat.bin")

    par.add_argument("--display",
                     dest="imgdisplay",
                     help="Display B-mode image? (True/False)",
                     type=bool,
                     default=False)

    par.add_argument("--save",
                     dest="imgsave",
                     help="Save B-mode image? (True/False)",
                     type=bool,
                     default=True)

    par.add_argument("--fn",
                     dest="savename",
                     help="Name of B-Mode image to be saved? (only applicable if saving image)",
                     type=str,
                     default='bmode.png'
                     )

    par.add_argument("--log",
                     dest="loglevel",
                     help="Set the Loglevel of Logger",
                     type=str,
                     default='DEBUG')

    args = par.parse_args()
    return args
