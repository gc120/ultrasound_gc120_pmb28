import numpy as np
from scipy.signal import hilbert
import logging
logging.basicConfig(filename="LogFiles/logsInfo.log", filemode='w', level=logging.DEBUG, format='%(asctime)s %(message)s')


def rectify(signal_d):
    """Changes the rfdata into positive values

    :param signal_d: The rfdata taken in
    :type: array
    :return: Returns the signal as all positive numbers
    :rtype: array

    """

    logging.debug("Performing envelope detection.")
    rectsig = np.abs(hilbert(signal_d))

    logging.info("Envelope-detected signal:" + str(rectsig))
    return rectsig
