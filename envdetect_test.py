import unittest


class EnvDetectUnitTest(unittest.TestCase):

    def test_env_detection(self):
        from envdetect import rectify
        signal = [1, 2, 3, 4, 3, 2, 1]
        rectified = rectify(signal)
        self.assertEquals(rectified[1], 2.3451030603178249)
        self.assertEquals(rectified[2], 3.2669144002972574)
