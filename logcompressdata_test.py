import unittest


class LogCompressDataTest(unittest.TestCase):

    def test_log_compress(self):
        from LogCompressData import log_compress
        import numpy as np
        input_arr = [1, 3, 19, 30, 6, 2]
        expected = [1.041392685158225, 1.1139433523068367, 1.462397997898956, 1.6020599913279625, 1.2041199826559248, 1.0791812460476249]
        compressed = log_compress(input_arr)
        print(compressed)

        np.testing.assert_equal(compressed, expected)
        #np.testing.assert_equal(peak.x_peaks, peak_test)

if __name__ == '__main__':
    unittest.main()