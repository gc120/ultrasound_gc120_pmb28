import ReadBinaryDataUltrasound as rb
from envdetect import *
import LogCompressData as lp
from outfxn import *
import args


def main(inpargs):
    # arguments
    arg = inpargs
    jsonfile = arg.jsonfile
    rfdata = arg.rfdata
    imgdisplay = arg.imgdisplay
    imgsave = arg.imgsave
    log = arg.loglevel
    savename = arg.savename

    # set loglevel
    while True:
        try:
            loglevel = getattr(logging, log.upper(), None)
            if not isinstance(loglevel, int):
                raise ValueError('Invalid log level: %s' % loglevel)
            logging.basicConfig(filename="LogFiles/logsInfo.log", filemode='w', format='%(asctime)s %(message)s', level=loglevel)
            break
        except ValueError:
            logging.critical('Value Error.. Enter correct Debug Level')
            log = input('Please try another filename: ')

    # Gather data from JSON file
    metadata = rb.read_json_file(jsonfile)
    speed = rb.get_speed(jsonfile)
    freq = rb.get_sampling_frequency(jsonfile)
    axial_samples = rb.get_axial_samples(jsonfile)
    num_beams = rb.get_num_beams(jsonfile)
    beam_spacing = rb.get_beam_spacing(jsonfile)

    # Read rfdata and run envelope detection/log compression, convert rfdata
    rf = rb.read_binary_file(rfdata)

    matrix = []
    for i in range(0, len(rf), axial_samples):
        beam = rf[i:i+axial_samples]  # get data for one beam
        rectdata = rectify(beam)  # rectify signal
        compressed = lp.log_compress(rectdata)  # do log compression
        rev = list(reversed(compressed))
        matrix.append(rev)

    # Image stuff
    mat = np.array(matrix)
    t = mat.transpose()
    dims = rb.get_dimensions(jsonfile)
    mwidth = dims['x']
    mheight = dims['y']

    img_plot(imgdisplay, mwidth, mheight, t, imgsave, savename)

if __name__ == '__main__':
    inpargs = args.getargs()
    main(inpargs)
