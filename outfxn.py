import matplotlib.pyplot as plt
import logging


def img_plot(imgdisplay, mwidth, mheight, picarray, imgsave, savename):
    """

    :param imgdisplay: Boolean value on whether or not to plot
    :param mwidth: Array of the x values of data
    :param mheight: Array of y values of data
    :param picarray: 2D matrix of depth values
    :param imgsave: Boolean value on whether or not to save the plot
    :param savename: User entered name of the new image file to be saved
    :return:

    """

    plt.gray()
    plt.contourf(mwidth, mheight, picarray, 200)
    plt.xlabel('Meters')
    plt.ylabel('Meters')
    plt.title(savename)

    if not imgdisplay:
        logging.info('Plot Not displayed')
        if imgsave:
            plt.savefig(savename, format="png")
            logging.info('Plot Saved as bmode.png')

    else:
        if imgsave:
            plt.savefig(savename, format="png")
            logging.info('Plot Saved')
        plt.show()
        logging.info('Plot displayed')

    return
