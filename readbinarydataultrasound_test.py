import unittest


class ReadBinaryDataUltrasoundTest(unittest.TestCase):

    def test_get_dimensions(self):
        from ReadBinaryDataUltrasound import get_dimensions
        dims = get_dimensions("bmode.json")
        x_test = [0.0, 0.00011746274509803921, 0.00023492549019607842, 0.00035238823529411766]
        y_test = [0.0, 7.7e-05, 0.000154, 0.000231]
        self.assertListEqual(dims['x'][:4], x_test)
        self.assertListEqual(dims['y'][:4], y_test)

    def test_read_binary_file(self):
        from ReadBinaryDataUltrasound import read_binary_file
        rfdata = read_binary_file("rfdat.bin")
        rf_expected = [ 0,  0,  0,  0,  0,  0,  0, -1,  0, -1]
        self.assertEquals(rfdata[9], rf_expected[9])

    def test_read_json_file(self):
        from ReadBinaryDataUltrasound import read_json_file
        json_data = read_json_file("bmode.json")
        self.assertEquals(json_data["c"], 1540)

    def test_get_speed(self):
        from ReadBinaryDataUltrasound import get_speed
        speed = get_speed("bmode.json")
        self.assertEquals(speed, 1540)

    def test_get_sampling_frequency(self):
        from ReadBinaryDataUltrasound import get_sampling_frequency
        freq = get_sampling_frequency("bmode.json")
        self.assertEquals(freq, 40000000)

    def test_get_axial_samples(self):
        from ReadBinaryDataUltrasound import get_axial_samples
        samples = get_axial_samples("bmode.json")
        self.assertEquals(samples, 1556)

    def test_get_beam_spacing(self):
        from ReadBinaryDataUltrasound import get_beam_spacing
        spacing = get_beam_spacing("bmode.json")
        self.assertEquals(spacing, 0.00011746274509803921)

    def test_get_num_beams(self):
        from ReadBinaryDataUltrasound import get_num_beams
        beams = get_num_beams("bmode.json")
        self.assertEquals(beams, 256)
# if __name__ == '__main__':
#     unittest.main()