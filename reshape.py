def matrify(num_beams, axial_samples, signal):
    """converts a numpy array into a 2d matrix while getting rid of the extra figures from convoluting

    :param num_beams: The number of lateral beams
    :param axial_samples: The number of samples in a depth
    :param signal: The signal to be converted into a matrix
    :return: returns a 2d matrix

    """
    #changes from numpy array into a 2d matrix of correct size
    remainder = len(signal) % (axial_samples*num_beams)

    #Because of convolution extra values are added to either end of the original signal that need to be cropped off
    cropsig = signal[round(remainder/2):len(signal)-round(remainder/2)]
    imgmatrix = cropsig.reshape(axial_samples, num_beams)

    return imgmatrix


def getwidth(beam_spacing, num_beams):
    """Gets the width of the frame in meters

    :param beam_spacing: The space between lateral beams
    :param num_beams: The number of lateral beams
    :return: Returns the width of the frame in meters
    """
    return beam_spacing * num_beams


def getheight(axial_samples, freq, speed):
    """

    :param axial_samples: The number of samples in a depth
    :param freq: The sampling frequency
    :param speed: The speed of sound in the tissue
    :return: Returns the height of the frame in meters

    """
    return (axial_samples * 1/freq * speed) / 2
